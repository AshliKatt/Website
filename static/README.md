# About
This repository holds compiled code for my website, [ashli.dev](https://www.ashli.dev/). 
You are most likely looking for the site's *source code*, which is at [this repository](https://codeberg.org/AshliKatt/Website).